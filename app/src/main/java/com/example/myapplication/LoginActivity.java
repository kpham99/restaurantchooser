package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import org.w3c.dom.Text;

public class LoginActivity extends AppCompatActivity {
    Button buttonLogin;
    TextView textViewRegisterAccount;
    TextInputLayout textInputTextEmail,textInputTextTextPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Get fields
        textInputTextEmail = findViewById(R.id.textInputTextEmail);
        textInputTextTextPassword = findViewById(R.id.textInputTextTextPassword);
        buttonLogin = findViewById(R.id.buttonLogin);
        textViewRegisterAccount = findViewById(R.id.textViewRegisterAccount);
        // Login Button
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Define strings for values in the textbox
                String emailInput = textInputTextEmail.getEditText().getText().toString().trim();
                String password = textInputTextTextPassword.getEditText().getText().toString().trim();
              // If email or password is empty
              if(emailInput.isEmpty()) {
                  textInputTextEmail.setError("Please fill in field");
              } else if(password.isEmpty()) {
                  textInputTextTextPassword.setError("Please fill in field");
              } else{
                  //TODO Check if user credentials with backend
                  Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                  startActivity(intent);
                }
            }
        });
        // Take user to register activity when clicked
        textViewRegisterAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}