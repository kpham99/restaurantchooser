package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.textfield.TextInputLayout;

public class RegisterActivity extends AppCompatActivity {

    TextInputLayout textInputTextName, textInputTextEmail, textInputTextPassword1,
            textInputTextPassword2;
    Button buttonSubmit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // Get fields
        textInputTextName = findViewById(R.id.textInputTextName);
        textInputTextEmail = findViewById(R.id.textInputTextEmail);
        textInputTextPassword1 = findViewById(R.id.textInputTextPassword1);
        textInputTextPassword2 = findViewById(R.id.textInputTextPassword2);
        // Button
        buttonSubmit = findViewById(R.id.buttonSubmit);
        // Submit button
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Define strings for values in the textbox
                String userName = textInputTextName.getEditText().getText().toString().trim();
                String email = textInputTextEmail.getEditText().getText().toString().trim();
                String password1 = textInputTextPassword1.getEditText().getText().toString().trim();
                String password2 = textInputTextPassword2.getEditText().getText().toString().trim();
                // Check if fields are empty
                if(userName.isEmpty()) {
                    textInputTextName.setError("Please fill in field");
                    textInputTextEmail.setError(null);
                    textInputTextPassword1.setError(null);
                }
                else if(email.isEmpty()) {
                    textInputTextName.setError(null);
                    textInputTextEmail.setError("Please fill in field");
                    textInputTextPassword1.setError(null);

                } else if (password1.isEmpty()) {
                    textInputTextName.setError(null);
                    textInputTextEmail.setError(null);
                    textInputTextPassword1.setError("Please fill in field");
                }
                else if(!password1.equals(password2) && !password1.equals("")) {
                    textInputTextPassword2.setError("Password does not match");
                }// If all fields have details and password match return return user to login activity
                else {
                    //TODO Add backend for inserting data
                    Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}